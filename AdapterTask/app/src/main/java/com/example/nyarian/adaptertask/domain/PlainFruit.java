package com.example.nyarian.adaptertask.domain;

import android.support.annotation.NonNull;

public class PlainFruit implements Fruit, Comparable<PlainFruit> {

    private final FruitType type;
    private final int weight;

    PlainFruit(FruitType type, int weight) {
        this.type = type;
        this.weight = weight;
    }

    @Override
    public String name() {
        return this.type.toString();
    }

    @Override
    public int weight() {
        return this.weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlainFruit that = (PlainFruit) o;

        if (weight != that.weight) {
            return false;
        }
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + weight;
        return result;
    }

    @Override
    public String toString() {
        return "PlainFruit{" +
                "type=" + type +
                ", weight=" + weight +
                '}';
    }

    @Override
    public int compareTo(@NonNull PlainFruit o) {
        final int nameCompareResult = this.name().compareTo(o.name());
        return nameCompareResult == 0 ? Integer.compare(this.weight, o.weight()) : nameCompareResult;
    }
}
