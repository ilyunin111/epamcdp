package com.example.andrii_iliunin.a2_2_recyclerview.ui.users;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.repository.UsersRepositoryFactory;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.user.UserDomainToUiConversion;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.ViewCommand;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.data.UsersDataSourceFactory;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.data.UsersUiRepository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

public class UsersViewModel extends ViewModel {

    private final LiveData<PagedList<UserUi>> usersListLiveData;
    private final MutableLiveData<Boolean> loadingStatusLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> titleLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> descriptionLiveData = new MutableLiveData<>();
    private final MediatorLiveData<NotificationParameters> mediatorLiveData;
    private final RemindCommand remindCommand;

    public UsersViewModel() {
        final UsersDataSourceFactory dataSourceFactory = new UsersDataSourceFactory(
                new UsersUiRepository(
                        new UsersRepositoryFactory().getRepository(), new UserDomainToUiConversion()
                ));

        final PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setPrefetchDistance(100)
                .setInitialLoadSizeHint(10)
                .setPageSize(10)
                .build();

        this.loadingStatusLiveData.setValue(true);
        this.usersListLiveData = new LivePagedListBuilder<>(dataSourceFactory, config).build();
        this.mediatorLiveData = new NotificationParametersMediatorLiveData(this.titleLiveData, this.descriptionLiveData);
        this.remindCommand = new RemindCommand(this);
    }

    public LiveData<PagedList<UserUi>> getUsersListLiveData() {
        return this.usersListLiveData;
    }

    public LiveData<Boolean> getLoadingStatusLiveData() {
        return this.loadingStatusLiveData;
    }

    LiveData<NotificationParameters> observeNotificationParameters() {
        return this.mediatorLiveData;
    }

    public ViewCommand remindCommand() {
        return this.remindCommand;
    }

    public void title(final String title) {
        this.titleLiveData.setValue(title);
    }

    public void description(final String description) {
        this.descriptionLiveData.setValue(description);
    }

    private static class NotificationParametersMediatorLiveData extends MediatorLiveData<NotificationParameters> {
        private final LiveData<String> titleLiveData;
        private final LiveData<String> descriptionLiveData;
        private String title;
        private String description;

        NotificationParametersMediatorLiveData(final LiveData<String> titleLiveData,
                                               final LiveData<String> descriptionLiveData) {
            this.titleLiveData = titleLiveData;
            this.descriptionLiveData = descriptionLiveData;
            this.merge();
        }

        private void merge() {
            this.addSource(this.titleLiveData, title -> {
                if (this.description != null) {
                    this.setValue(new NotificationParameters(title, this.description));
                    this.reset();
                } else {
                    this.title = title;
                }
            });
            this.addSource(this.descriptionLiveData, description -> {
                if (this.description != null) {
                    this.setValue(new NotificationParameters(this.title, description));
                    this.reset();
                } else {
                    this.description = description;
                }
            });
        }

        private void reset() {
            this.title = null;
            this.description = null;
        }
    }
}