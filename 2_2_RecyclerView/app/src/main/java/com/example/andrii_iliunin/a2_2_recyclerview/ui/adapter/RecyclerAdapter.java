package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public abstract class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder<?>> {

    private final ViewHolderFactory factory;
    private final AbstractChainedBindStrategy bindStrategy;

    protected RecyclerAdapter(final ViewHolderFactory factory, final AbstractChainedBindStrategy bindStrategy) {
        this.factory = factory;
        this.bindStrategy = bindStrategy;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return this.factory.createHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        this.bindStrategy.bind(holder, position, Collections.emptyList());
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder<?> holder, final int position, final List<Object> payloads) {
        this.bindStrategy.bind(holder, position, payloads);
    }
}
