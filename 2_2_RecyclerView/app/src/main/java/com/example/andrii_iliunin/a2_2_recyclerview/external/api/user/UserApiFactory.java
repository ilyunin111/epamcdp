package com.example.andrii_iliunin.a2_2_recyclerview.external.api.user;

import com.example.andrii_iliunin.a2_2_recyclerview.external.api.RestFactory;

public class UserApiFactory {

    public UserApi create() {
        return new RestFactory<>(UserApi.class).create();
    }

}
