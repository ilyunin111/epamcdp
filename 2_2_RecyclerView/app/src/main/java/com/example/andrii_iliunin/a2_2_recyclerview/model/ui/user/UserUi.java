package com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.UiModel;

import androidx.recyclerview.widget.DiffUtil;

public class UserUi implements UiModel {

    public static final DiffUtil.ItemCallback<UserUi> DIFF_UTIL_CALLBACK = new DiffUtil.ItemCallback<UserUi>() {
        @Override
        public boolean areItemsTheSame(final UserUi oldItem, final UserUi newItem) {
            return oldItem.getName().equals(newItem.getName());
        }

        @Override
        public boolean areContentsTheSame(final UserUi oldItem, final UserUi newItem) {
            return oldItem.equals(newItem);
        }
    };

    private final String id;
    private final String name;
    private final PictureUi picture;
    private final AddressUi address;

    public UserUi(final String id, final String name, final PictureUi picture, final AddressUi address) {
        this.id = id == null ? "" : id;
        this.name = name;
        this.picture = picture;
        this.address = address;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public PictureUi getPicture() {
        return this.picture;
    }

    public AddressUi getAddress() {
        return this.address;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        final UserUi userUi = (UserUi) o;

        if (this.name != null ? !this.name.equals(userUi.name) : userUi.name != null) {
            return false;
        }
        if (this.picture != null ? !this.picture.equals(userUi.picture) : userUi.picture != null) {
            return false;
        }
        return this.address != null ? this.address.equals(userUi.address) : userUi.address == null;
    }

    @Override
    public int hashCode() {
        int result = this.name != null ? this.name.hashCode() : 0;
        result = 31 * result + (this.picture != null ? this.picture.hashCode() : 0);
        result = 31 * result + (this.address != null ? this.address.hashCode() : 0);
        return result;
    }
}
