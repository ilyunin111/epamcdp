package com.nyarian.android.cdp.game;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class PlainCombinationSearch implements CombinationSearch {

    private static final Collection<BiPredicate<Position, Position>> FILTERS =
            Arrays.asList((position, lastPosition) -> position.getX() == lastPosition.getX(),
                    (position, lastPosition) -> position.getY() == lastPosition.getY());

    @Override
    public Combination find(final Collection<Position> positions,
                            final Position lastPosition,
                            final Sign sign,
                            final int boardSize) {
        Combination result = Combination.NO_COMBINATION;
        if (positions.size() >= boardSize - 1) {
            for (final BiPredicate<Position, Position> filter : FILTERS) {
                final Collection<Position> filteredPositions = this.getPositions(positions, lastPosition, filter);
                if (filteredPositions.size() == boardSize - 1) {
                    result = new Combination(filteredPositions, sign, true);
                    break;
                }
            }
            if (result == Combination.NO_COMBINATION) {
                final Collection<Position> diagonal = this.getDiagonal(positions, lastPosition, boardSize);
                if (diagonal.size() == boardSize + 1) {
                    result = new Combination(diagonal, sign, true);
                }
            }
        }
        return result;
    }

    private Collection<Position> getPositions(final Collection<Position> allPositions,
                                              final Position lastPosition,
                                              final BiPredicate<Position, Position> filter) {
        return allPositions
                .stream()
                .filter(position -> filter.test(position, lastPosition))
                .collect(Collectors.toList());
    }

    private Collection<Position> getDiagonal(final Collection<Position> positions,
                                             final Position lastPosition,
                                             final int boardSize) {
        final Collection<Position> result;
        final int expectedSize = boardSize + 1;
        if (lastPosition.getX() == lastPosition.getY()) {
            result = positions
                    .stream()
                    .filter(position -> position.getX() == position.getY())
                    .collect(Collectors.toList());
        } else if (lastPosition.getX() + lastPosition.getY() == expectedSize) {
            result = positions
                    .stream()
                    .filter(position -> position.getX() + position.getY() == expectedSize)
                    .collect(Collectors.toList());
        } else {
            result = Collections.emptyList();
        }
        return result;
    }

}
