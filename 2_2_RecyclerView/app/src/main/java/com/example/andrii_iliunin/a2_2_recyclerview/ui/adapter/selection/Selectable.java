package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection;

public interface Selectable {

    void select();

    void unselect();

    boolean isSelected();

}
