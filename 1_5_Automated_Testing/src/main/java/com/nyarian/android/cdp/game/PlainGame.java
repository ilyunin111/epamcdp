package com.nyarian.android.cdp.game;

import com.nyarian.android.cdp.player.Player;
import com.nyarian.android.cdp.print.Greeter;
import com.nyarian.android.cdp.print.Printer;
import com.nyarian.android.cdp.turn.FirstTurnStrategy;

import java.util.stream.Collectors;

public class PlainGame implements Game {

    private final Player first;
    private final Player second;
    private final Board board;
    private final Printer printer;
    private final Greeter greeter;
    private final CombinationSearch combinationSearch;
    private final FirstTurnStrategy firstTurnStrategy;
    private Sign lastWinner = Sign.NONE;

    public PlainGame(final Player first,
                     final Player second,
                     final Board board,
                     final Printer printer,
                     final Greeter greeter,
                     final CombinationSearch search,
                     final FirstTurnStrategy firstTurnStrategy,
                     final SignAssignmentStrategy signAssignmentStrategy) {
        signAssignmentStrategy.assign(first, second);
        this.first = first;
        this.second = second;
        this.board = board;
        this.printer = printer;
        this.greeter = greeter;
        this.combinationSearch = search;
        this.firstTurnStrategy = firstTurnStrategy;
    }

    @Override
    public void start() {
        this.printer.display(this.board);
        do {
            this.next().turn(this.board);
            this.printer.display(this.board);
        } while (!this.combinationSearch.find(
                this.board.moves(this.board.whoseTurn().opposite())
                        .stream()
                        .filter(move -> !move.position().equals(this.board.lastMove().position()))
                        .map(Move::position)
                        .collect(Collectors.toList()),
                this.board.lastMove().position(),
                this.board.lastMove().sign(),
                this.board.size())
                .hasWon());
        this.performGreeting();
    }

    private void performGreeting() {
        this.lastWinner = this.board.whoseTurn().opposite();
        final Player winner = this.first.sign() == this.lastWinner ? this.first : this.second;
        final Player looser = this.first.equals(winner) ? this.second : this.first;
        this.greeter.greet(winner, looser);
    }

    private Player next() {
        final Sign whoseTurn = this.board.whoseTurn();
        if (whoseTurn == Sign.NONE) {
            return this.firstTurnStrategy.whoTurnsFirst(this.first, this.second, this.lastWinner);
        } else {
            return whoseTurn == this.first.sign() ? this.first : this.second;
        }
    }

    @Override
    public void reset() {
        this.board.reset();
    }
}
