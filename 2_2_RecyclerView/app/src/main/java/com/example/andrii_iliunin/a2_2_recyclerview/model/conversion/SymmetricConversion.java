package com.example.andrii_iliunin.a2_2_recyclerview.model.conversion;

import java.util.List;

public interface SymmetricConversion<T, R> extends Conversion<T, R> {

    T reverse(R source);

    List<T> reverse(Iterable<R> source);

}
