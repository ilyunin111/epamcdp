package com.example.nyarian.adaptertask.domain;

import android.annotation.SuppressLint;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

public class FruitDataSource implements DataSource<PersistedFruit> {

    private final Random random;

    public FruitDataSource(Random random) {
        this.random = random;
    }

    @Override
    public Collection<PersistedFruit> request() {
        return Arrays.asList(
                this.getRandomFruit(),
                this.getRandomFruit(),
                this.getRandomFruit(),
                this.getRandomFruit(),
                this.getRandomFruit()
        );
    }

    @SuppressLint("DefaultLocale")
    private PersistedFruit getRandomFruit() {
        return new PlainPersistedFruit(
                new PlainFruit(
                        Arrays.asList(FruitType.values()).get(this.random.nextInt(3)),
                        random.nextInt(5) + 1
                )
        );
    }
}
