
package com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationDto {

    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("coordinates")
    @Expose
    private CoordinatesUserAddressDto coordinatesUserAddressDto;

    public String getStreet() {
        return this.street;
    }

    public void setStreet(final String street) {
        this.street = street;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getState() {
        return this.state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public void setPostcode(final String postcode) {
        this.postcode = postcode;
    }

    public CoordinatesUserAddressDto getCoordinatesUserAddressDto() {
        return this.coordinatesUserAddressDto;
    }

    public void setCoordinatesUserAddressDto(final CoordinatesUserAddressDto coordinatesUserAddressDto) {
        this.coordinatesUserAddressDto = coordinatesUserAddressDto;
    }

}
