package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import java.util.List;

public abstract class AbstractChainedBindStrategy implements ChainedBindStrategy {

    private static final ChainedBindStrategy EMPTY = new AbstractChainedBindStrategy() {
        @Override
        protected boolean isSuitable(final int viewType) {
            throw new UnsupportedOperationException("#isSuitable()");
        }

        @Override
        protected void process(final RecyclerViewHolder recyclerViewHolder, final int position, final List payload) {
            throw new UnsupportedOperationException("#process()");
        }
    };

    private final ChainedBindStrategy next;

    protected AbstractChainedBindStrategy() {
        this.next = EMPTY;
    }

    public AbstractChainedBindStrategy(final ChainedBindStrategy next) {
        this.next = next;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void bind(final RecyclerViewHolder recyclerViewHolder,
                     final int position,
                     final List<Object> payload) {
        if (this.isSuitable(recyclerViewHolder.getViewType())) {
            this.process(recyclerViewHolder, position, payload);
        } else {
            this.next.bind(recyclerViewHolder, position, payload);
        }
    }

    protected abstract boolean isSuitable(int viewType);

    protected abstract void process(final RecyclerViewHolder recyclerViewHolder,
                                    final int position,
                                    final List<Object> payload);

}
