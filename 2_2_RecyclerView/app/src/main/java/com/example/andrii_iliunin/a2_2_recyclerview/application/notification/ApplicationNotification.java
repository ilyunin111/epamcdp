package com.example.andrii_iliunin.a2_2_recyclerview.application.notification;

import java.util.concurrent.TimeUnit;

import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

public interface ApplicationNotification {

    void make();

    class Builder {
        private static final int ONE_TIME_NOTIFICATION = 1;
        public static final long PERIOD_DEFAULT = 1L;

        private final String title;
        private final String description;
        private boolean repeated = false;
        private long repeatPeriod = PERIOD_DEFAULT;
        private TimeUnit timeUnit = TimeUnit.MINUTES;

        public Builder(final String title, final String description) {
            this.title = title;
            this.description = description;
        }

        public Builder oneShot() {
            this.repeated = false;
            return this;
        }

        public Builder repeated() {
            this.repeated = true;
            return this;
        }

        public Builder repeat(final long period, final TimeUnit timeUnit) {
            this.repeatPeriod = period;
            this.timeUnit = timeUnit;
            return this.repeated();
        }

        public ApplicationNotification build() {
            final WorkRequest work;
            if (this.repeated) {
                work = new PeriodicWorkRequest
                        .Builder(NotificationWorker.class, 1L, TimeUnit.MINUTES)
                        .build();
            } else {
                work = new OneTimeWorkRequest.Builder(NotificationWorker.class).build();
            }
            return new WorkerNotification(work, WorkManager.getInstance());
        }
    }

}
