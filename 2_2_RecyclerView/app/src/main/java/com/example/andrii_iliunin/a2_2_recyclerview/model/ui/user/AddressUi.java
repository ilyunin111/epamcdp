package com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.UiModel;

public class AddressUi implements UiModel {

    private final String state;
    private final String city;
    private final String street;

    public AddressUi(final String state, final String city, final String street) {
        this.state = state;
        this.city = city;
        this.street = street;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        final AddressUi addressUi = (AddressUi) o;

        if (this.state != null ? !this.state.equals(addressUi.state) : addressUi.state != null) {
            return false;
        }
        if (this.city != null ? !this.city.equals(addressUi.city) : addressUi.city != null) {
            return false;
        }
        return this.street != null ? this.street.equals(addressUi.street) : addressUi.street == null;
    }

    @Override
    public int hashCode() {
        int result = this.state != null ? this.state.hashCode() : 0;
        result = 31 * result + (this.city != null ? this.city.hashCode() : 0);
        result = 31 * result + (this.street != null ? this.street.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s, %s, %s", this.state, this.city, this.street);
    }
}
