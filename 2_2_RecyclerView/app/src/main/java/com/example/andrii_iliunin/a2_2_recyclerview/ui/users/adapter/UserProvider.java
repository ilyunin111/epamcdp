package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

public interface UserProvider {

    UserUi get(int position);

}
