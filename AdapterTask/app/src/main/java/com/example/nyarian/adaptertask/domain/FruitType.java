package com.example.nyarian.adaptertask.domain;

public enum  FruitType {
    APPLE {
        @Override
        public String toString() {
            return "Apple";
        }
    }, ORANGE {
        @Override
        public String toString() {
            return "Orange";
        }
    }, BANANA {
        @Override
        public String toString() {
            return "Banana";
        }
    };

    public abstract String toString();
}
