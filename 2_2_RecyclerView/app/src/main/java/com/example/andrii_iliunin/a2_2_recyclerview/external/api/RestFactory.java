package com.example.andrii_iliunin.a2_2_recyclerview.external.api;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestFactory<T> {

    private static final Retrofit DEFAULT_RETROFIT_ABSTRACTION;

    static {
        final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        DEFAULT_RETROFIT_ABSTRACTION = new Retrofit.Builder()
                .baseUrl("https://randomuser.me/api/")
                .client(new OkHttpClient.Builder()
                        .addInterceptor(httpLoggingInterceptor)
                        .build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }

    private final Class<T> interfaceToWrap;

    public RestFactory(final Class<T> interfaceToWrap) {
        this.interfaceToWrap = interfaceToWrap;
    }

    public T create() {
        return DEFAULT_RETROFIT_ABSTRACTION.create(this.interfaceToWrap);
    }

}
