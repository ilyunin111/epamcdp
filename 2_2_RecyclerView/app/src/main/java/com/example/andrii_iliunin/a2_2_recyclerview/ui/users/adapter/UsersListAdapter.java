package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import android.view.ViewGroup;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.ProgressBindStrategy;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.RecyclerViewHolder;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.ViewHolderFactory;

import java.util.Collections;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.selection.SelectionTracker;

public class UsersListAdapter extends PagedListAdapter<UserUi, RecyclerViewHolder> implements UserProvider {

    static final int USER_VIEW_TYPE = RecyclerViewHolder.PROGRESS_VIEW_TYPE + 1;

    private final ViewHolderFactory factory = new UsersViewHolderFactory();
    private final UserBindStrategy bindStrategy;

    public UsersListAdapter(final LiveData<Boolean> loadStatus) {
        super(UserUi.DIFF_UTIL_CALLBACK);
        final ProgressBindStrategy progressChain = new ProgressBindStrategy(loadStatus);
        this.bindStrategy = new UserBindStrategy(progressChain, this);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return this.factory.createHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {
        this.bindStrategy.bind(holder, position, Collections.emptyList());
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position, final List<Object> payloads) {
        this.bindStrategy.bind(holder, position, payloads);
    }

    @Override
    public int getItemViewType(final int position) {
        return position == this.getItemCount() ? RecyclerViewHolder.PROGRESS_VIEW_TYPE : USER_VIEW_TYPE;
    }

    @Override
    public UserUi get(final int position) {
        return this.getItem(position);
    }

    public void setTracker(final SelectionTracker<String> tracker) {
        this.bindStrategy.setSelectionTracker(tracker);
    }
}
