package com.example.andrii_iliunin.a2_2_recyclerview.domain.user;

import java.math.BigDecimal;

public class Coordinates {

    public static final Coordinates EMPTY = new Coordinates(BigDecimal.ZERO, BigDecimal.ZERO);
    private final BigDecimal latitude;
    private final BigDecimal longitude;

    public Coordinates(final BigDecimal latitude, final BigDecimal longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
