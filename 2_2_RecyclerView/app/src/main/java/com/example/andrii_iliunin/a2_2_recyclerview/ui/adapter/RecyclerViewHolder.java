package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import android.view.View;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public abstract class RecyclerViewHolder<T> extends RecyclerView.ViewHolder {

    public static final int PROGRESS_VIEW_TYPE = 1;

    public RecyclerViewHolder(final View itemView) {
        super(itemView);
    }

    public abstract int getViewType();

    public abstract void bind(final T source, final List<Object> payloads);

}
