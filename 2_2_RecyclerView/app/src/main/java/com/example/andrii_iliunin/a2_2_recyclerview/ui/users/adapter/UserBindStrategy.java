package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.AbstractChainedBindStrategy;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.ChainedBindStrategy;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.RecyclerViewHolder;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection.Selectable;

import java.util.List;

import androidx.recyclerview.selection.SelectionTracker;

public class UserBindStrategy extends AbstractChainedBindStrategy {

    private final UserProvider usersListLiveData;
    private SelectionTracker<String> selectionTracker;

    public UserBindStrategy(final UserProvider usersListLiveData) {
        super();
        this.usersListLiveData = usersListLiveData;
    }

    UserBindStrategy(final ChainedBindStrategy next, final UserProvider usersListLiveData) {
        super(next);
        this.usersListLiveData = usersListLiveData;
    }

    public void setSelectionTracker(final SelectionTracker<String> selectionTracker) {
        this.selectionTracker = selectionTracker;
    }

    @Override
    protected boolean isSuitable(final int viewType) {
        return viewType == UsersListAdapter.USER_VIEW_TYPE;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void process(final RecyclerViewHolder recyclerViewHolder,
                           final int position,
                           final List<Object> payload) {
        final UserUi source = this.usersListLiveData.get(position);
        if (payload.contains(SelectionTracker.SELECTION_CHANGED_MARKER)) {
            this.processSelection((Selectable) recyclerViewHolder, source);
        } else {
            recyclerViewHolder.bind(source, payload);
            this.processSelection((Selectable) recyclerViewHolder, source);
        }
    }

    private void processSelection(final Selectable recyclerViewHolder, final UserUi source) {
        if (this.selectionTracker.isSelected(source.getId())) {
            recyclerViewHolder.select();
        } else {
            recyclerViewHolder.unselect();
        }
    }
}
