package com.nyarian.android.cdp.turn;

import com.nyarian.android.cdp.game.Sign;
import com.nyarian.android.cdp.player.Player;

public class StrategySignChecker {

    private static final String ERROR_SOURCE_FIRST_PLAYER = "first player";
    private static final String ERROR_SOURCE_SECOND_PLAYER = "second player";
    private static final String ERROR_SOURCE_LAST_WINNER = "last winner";

    void examineSigns(final Player first, final Player second, final Sign lastWinner) {
        this.checkSign(first.sign(), ERROR_SOURCE_FIRST_PLAYER);
        this.checkSign(second.sign(), ERROR_SOURCE_SECOND_PLAYER);
        if (lastWinner == null) {
            throw new IllegalArgumentException(String.format("Sign passed by %s reference is null.", ERROR_SOURCE_LAST_WINNER));
        }
    }

    private void checkSign(final Sign sign, final String source) {
        if (sign == null) {
            throw new IllegalArgumentException(String.format("Sign passed by %s reference is null.", source));
        }
        if (sign == Sign.NONE) {
            throw new IllegalArgumentException(String.format("Sign passed by %s is Sign.NONE", source));
        }
    }

}
