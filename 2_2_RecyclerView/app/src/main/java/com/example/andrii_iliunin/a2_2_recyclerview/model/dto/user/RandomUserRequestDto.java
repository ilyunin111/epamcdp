
package com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RandomUserRequestDto {

    @SerializedName("results")
    @Expose
    private List<UserDto> userDtos = new ArrayList<>();
    @SerializedName("info")
    @Expose
    private RequestInfoDto requestInfoDto;

    public List<UserDto> getUserResponse() {
        return this.userDtos;
    }

    public void setUserResponse(final List<UserDto> userDtos) {
        this.userDtos = userDtos;
    }

    public RequestInfoDto getRequestInfoDto() {
        return this.requestInfoDto;
    }

    public void setRequestInfoDto(final RequestInfoDto requestInfoDto) {
        this.requestInfoDto = requestInfoDto;
    }

}
