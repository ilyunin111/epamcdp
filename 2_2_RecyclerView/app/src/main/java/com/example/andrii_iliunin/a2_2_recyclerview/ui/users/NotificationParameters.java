package com.example.andrii_iliunin.a2_2_recyclerview.ui.users;

public class NotificationParameters {

    private String title;
    private String description;

    NotificationParameters(final String title, final String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setDescription(final String description) {
        this.description = description;
    }


}
