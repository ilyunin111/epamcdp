package com.example.nyarian.adaptertask.domain;

import android.support.annotation.NonNull;

public class PlainPersistedFruit implements PersistedFruit, Comparable<PlainPersistedFruit> {

    private final Fruit fruit;

    PlainPersistedFruit(PlainFruit fruit) {
        this.fruit = fruit;
    }

    @Override
    public long getId() {
        return this.fruit.hashCode() + Double.doubleToLongBits(this.fruit.weight());
    }

    @Override
    public String name() {
        return this.fruit.name();
    }

    @Override
    public int weight() {
        return this.fruit.weight();
    }

    @Override
    public String toString() {
        return this.fruit.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlainPersistedFruit that = (PlainPersistedFruit) o;

        return fruit != null ? fruit.equals(that.fruit) : that.fruit == null;
    }

    @Override
    public int hashCode() {
        return fruit != null ? fruit.hashCode() : 0;
    }

    @Override
    public int compareTo(@NonNull PlainPersistedFruit o) {
        final int nameCompareResult = this.name().compareTo(o.name());
        return nameCompareResult == 0 ? Integer.compare(this.weight(), o.weight()) : nameCompareResult;
    }
}
