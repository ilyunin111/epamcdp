package com.example.andrii_iliunin.a2_2_recyclerview.ui.users;

import com.example.andrii_iliunin.a2_2_recyclerview.application.notification.ApplicationNotification;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.ViewCommand;

public class RemindCommand implements ViewCommand {

    private final UsersViewModel viewModel;

    public RemindCommand(final UsersViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void execute() {
        final NotificationParameters notificationParameters = this.viewModel
                .observeNotificationParameters().getValue();
        new ApplicationNotification.Builder(notificationParameters.getTitle(),
                notificationParameters.getDescription())
                .oneShot()
                .build()
                .make();
    }

    @Override
    public boolean canExecute() {
        return true;
    }
}
