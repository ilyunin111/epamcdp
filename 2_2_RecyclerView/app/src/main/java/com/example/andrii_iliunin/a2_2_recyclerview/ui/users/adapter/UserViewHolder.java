package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andrii_iliunin.a2_2_recyclerview.R;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.RecyclerViewHolder;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection.Detailed;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection.Selectable;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.recyclerview.selection.ItemDetailsLookup;

class UserViewHolder extends RecyclerViewHolder<UserUi> implements Detailed<String>, Selectable {

    private final CheckBox checkBox;
    private final ImageView photoView;
    private final TextView nameView;
    private final TextView addressView;
    private String key;
    private boolean selected;

    UserViewHolder(final View itemView) {
        super(itemView);
        this.photoView = itemView.findViewById(R.id.photo);
        this.nameView = itemView.findViewById(R.id.name);
        this.addressView = itemView.findViewById(R.id.address);
        this.checkBox = itemView.findViewById(R.id.checkbox);
    }

    @Override
    public int getViewType() {
        return UsersListAdapter.USER_VIEW_TYPE;
    }

    @Override
    public void bind(final UserUi source, final List<Object> payloads) {
        this.key = source.getId();
        source.getPicture().loadMedium(Picasso.get(), this.photoView);
        this.nameView.setText(source.getName());
        this.addressView.setText(source.getAddress().toString());
    }

    @Override
    public ItemDetailsLookup.ItemDetails<String> getItemDetails() {
        final String localKey = this.key;
        return new ItemDetailsLookup.ItemDetails<String>() {
            @Override
            public int getPosition() {
                return UserViewHolder.this.getAdapterPosition();
            }

            @Override
            public String getSelectionKey() {
                return localKey;
            }

            @Override
            public boolean inSelectionHotspot(final MotionEvent e) {
                return true;
            }
        };
    }

    @Override
    public void select() {
        this.selected = true;
        this.checkBox.setChecked(true);
    }

    @Override
    public void unselect() {
        this.selected = false;
        this.checkBox.setChecked(false);
    }

    @Override
    public boolean isSelected() {
        return this.selected;
    }
}
