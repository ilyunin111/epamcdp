package com.example.andrii_iliunin.a2_2_recyclerview.domain.user;

public class Address {

    public static final Address EMPTY = new Address("", "", "", "", Coordinates.EMPTY);
    private final String street;
    private final String city;
    private final String state;
    private final String postcode;
    private final Coordinates coordinates;

    public Address(final String street, final String city, final String state, final String postcode, final Coordinates coordinates) {
        this.street = street;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.coordinates = coordinates;
    }

    public String getStreet() {
        return this.street;
    }

    public String getCity() {
        return this.city;
    }

    public String getState() {
        return this.state;
    }

    public String getPostcode() {
        return this.postcode;
    }

    public Coordinates getCoordinates() {
        return this.coordinates;
    }
}
