package com.example.andrii_iliunin.a2_2_recyclerview.domain.user;

public class Picture {

    public static final Picture EMPTY = new Picture("", "", "");
    private final String largePictureUrl;
    private final String mediumPictureUrl;
    private final String thumbnailPictureUrl;

    public Picture(final String largePictureUrl, final String mediumPictureUrl, final String thumbnailPictureUrl) {
        this.largePictureUrl = largePictureUrl;
        this.mediumPictureUrl = mediumPictureUrl;
        this.thumbnailPictureUrl = thumbnailPictureUrl;
    }

    public String getLargePictureUrl() {
        return this.largePictureUrl;
    }

    public String getMediumPictureUrl() {
        return this.mediumPictureUrl;
    }

    public String getThumbnailPictureUrl() {
        return this.thumbnailPictureUrl;
    }
}
