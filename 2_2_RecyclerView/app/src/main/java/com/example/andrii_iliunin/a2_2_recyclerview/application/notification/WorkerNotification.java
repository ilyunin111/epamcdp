package com.example.andrii_iliunin.a2_2_recyclerview.application.notification;

import androidx.work.WorkManager;
import androidx.work.WorkRequest;

class WorkerNotification implements ApplicationNotification {

    private final WorkRequest workRequest;
    private final WorkManager workManager;

    WorkerNotification(final WorkRequest workRequest, final WorkManager workManager) {
        this.workRequest = workRequest;
        this.workManager = workManager;
    }

    @Override
    public void make() {
        this.workManager.enqueue(this.workRequest);
    }
}
