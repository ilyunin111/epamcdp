package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import android.view.View;

import com.example.andrii_iliunin.a2_2_recyclerview.R;

import java.util.List;

public class ProgressViewHolder extends RecyclerViewHolder<Boolean> {

    private final View progressBar;

    public ProgressViewHolder(final View itemView) {
        super(itemView);
        this.progressBar = itemView.findViewById(R.id.progress_view);
    }

    @Override
    public int getViewType() {
        return RecyclerViewHolder.PROGRESS_VIEW_TYPE;
    }

    @Override
    public void bind(final Boolean source, final List<Object> payloads) {
        this.progressBar.setVisibility(source ? View.VISIBLE : View.GONE);
    }

}
