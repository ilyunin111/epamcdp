package com.nyarian.android.cdp.game;

import com.nyarian.android.cdp.player.Player;
import com.nyarian.android.cdp.print.Greeter;
import com.nyarian.android.cdp.print.Printer;
import com.nyarian.android.cdp.turn.FirstTurnStrategy;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import static com.nyarian.android.cdp.game.Sign.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PlainGameTest implements TestOf<PlainGame> {

    private static final boolean WON = true;
    private static final boolean NOT_WON = false;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    private Player first;
    @Mock
    private Player second;
    @Mock
    private Board board;
    @Mock
    private Printer printer;
    @Mock
    private Greeter greeter;
    @Mock
    private CombinationSearch search;
    @Mock
    private FirstTurnStrategy strategy;
    @Mock
    private Combination combination;
    @Mock
    private Move lastMove;
    @Mock
    private SignAssignmentStrategy signAssignmentStrategy;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void constructor_verifySignAssignmentWasRequested() {
        this.subject();
        verify(this.signAssignmentStrategy).assign(any(), any());
    }

    @Test
    public void start_assertFirstTurningPlayerWasAskedToTurnFirst_inFirstPlayerCase() {
        doReturn(this.first).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        inOrder(this.first, this.second).verify(this.first).turn(any());
    }

    @Test
    public void start_assertFirstTurningPlayerWasAskedToTurnFirst_inSecondPlayerCase() {
        doReturn(this.second).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        inOrder(this.first, this.second).verify(this.second).turn(any());
    }

    @Test
    public void start_verifyPrinterDisplayedTheBoardTimesEqualToPlayersTurns_plusOneTimeInitializing() {
        final Random random = new Random();
        final AtomicInteger turnsCounter = new AtomicInteger();
        doAnswer(invocationOnMock -> {
            turnsCounter.incrementAndGet();
            return random.nextBoolean();
        }).when(this.first).turn(any());
        doAnswer(invocationOnMock -> {
            turnsCounter.incrementAndGet();
            return random.nextBoolean();
        }).when(this.second).turn(any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(NOT_WON).doReturn(NOT_WON).doReturn(NOT_WON).doReturn(WON).when(this.combination).hasWon();
        doReturn(CROSS).doReturn(NOUGHT).doReturn(CROSS).doReturn(NOUGHT).doReturn(CROSS).doReturn(NOUGHT)
                .when(this.board).whoseTurn();
        doReturn(CROSS).when(this.first).sign();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        verify(this.printer, times(turnsCounter.incrementAndGet())).display(any());
    }

    @Test
    public void start_verifyFirstPlayerWasGreeted_ifFirstPlayerWon() {
        doReturn(this.first).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(NOUGHT).when(this.first).sign();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        verify(this.greeter).greet(eq(this.first), any());
    }

    @Test
    public void start_verifyFirstPlayerWasPitied_ifFirstPlayerLost() {
        doReturn(this.first).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(CROSS).when(this.first).sign();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        verify(this.greeter).greet(any(), eq(this.first));
    }

    @Test
    public void start_verifySecondPlayerWasGreeted_ifSecondPlayerWon() {
        doReturn(this.first).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(CROSS).when(this.first).sign();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        verify(this.greeter).greet(eq(this.second), any());
    }

    @Test
    public void start_verifySecondPlayerWasPitied_ifSecondPlayerLost() {
        doReturn(this.first).when(this.strategy).whoTurnsFirst(any(), any(), any());
        doReturn(this.combination).when(this.search).find(any(), any(), any(), anyInt());
        doReturn(WON).when(this.combination).hasWon();
        doReturn(NONE).doReturn(CROSS).when(this.board).whoseTurn();
        doReturn(NOUGHT).when(this.first).sign();
        doReturn(this.lastMove).when(this.board).lastMove();
        this.subject().start();
        verify(this.greeter).greet(any(), eq(this.second));
    }

    @Test
    public void reset_verifyBoardGotReset() {
        this.subject().reset();
        verify(this.board).reset();
    }

    @Override
    public PlainGame subject() {
        return new PlainGame(this.first,
                this.second,
                this.board,
                this.printer,
                this.greeter,
                this.search,
                this.strategy,
                this.signAssignmentStrategy);
    }
}