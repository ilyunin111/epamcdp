package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import android.view.ViewGroup;

public interface ViewHolderFactory {

    RecyclerViewHolder createHolder(ViewGroup parent, int viewType);

}
