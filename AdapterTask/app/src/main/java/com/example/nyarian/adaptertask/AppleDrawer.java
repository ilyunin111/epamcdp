package com.example.nyarian.adaptertask;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nyarian.adaptertask.domain.FruitType;
import com.example.nyarian.adaptertask.domain.PersistedFruit;
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate;

import java.util.List;

public class AppleDrawer extends AbsListItemAdapterDelegate<PersistedFruit, PersistedFruit, AppleDrawer.AppleHolder> {

    private final LayoutInflater inflater;

    AppleDrawer(final Activity activity) {
        this.inflater = activity.getLayoutInflater();
    }

    @Override
    protected boolean isForViewType(@NonNull PersistedFruit item, @NonNull List<PersistedFruit> items, int position) {
        return item.name().equals(FruitType.APPLE.toString());
    }

    @NonNull
    @Override
    protected AppleHolder onCreateViewHolder(@NonNull ViewGroup parent) {
        return new AppleHolder(this.inflater.inflate(R.layout.item_fruit, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull PersistedFruit item,
                                    @NonNull AppleHolder viewHolder,
                                    @NonNull List<Object> payloads) {
        viewHolder.getWeightView().setText(String.valueOf(item.weight()));
    }

    static class AppleHolder extends RecyclerView.ViewHolder {

        private final TextView weightView;

        AppleHolder(View itemView) {
            super(itemView);
            final ImageView photoView = itemView.findViewById(R.id.fruit_photo);
            photoView.setImageResource(R.drawable.ic_apple);
            final TextView nameView = itemView.findViewById(R.id.fruit_name);
            nameView.setText(FruitType.APPLE.toString());
            this.weightView = itemView.findViewById(R.id.fruit_weight);
        }

        public TextView getWeightView() {
            return weightView;
        }
    }
}
