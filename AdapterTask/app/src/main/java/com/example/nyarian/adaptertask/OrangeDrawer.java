package com.example.nyarian.adaptertask;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nyarian.adaptertask.domain.FruitType;
import com.example.nyarian.adaptertask.domain.PersistedFruit;
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate;

import java.util.List;

public class OrangeDrawer extends AbsListItemAdapterDelegate<PersistedFruit, PersistedFruit, OrangeDrawer.OrangeHolder> {

    private final LayoutInflater inflater;

    OrangeDrawer(final Activity activity) {
        this.inflater = activity.getLayoutInflater();
    }

    @Override
    protected boolean isForViewType(@NonNull PersistedFruit item, @NonNull List<PersistedFruit> items, int position) {
        return item.name().equals(FruitType.ORANGE.toString());
    }

    @NonNull
    @Override
    protected OrangeHolder onCreateViewHolder(@NonNull ViewGroup parent) {
        return new OrangeHolder(this.inflater.inflate(R.layout.item_fruit, parent, false));
    }

    @Override
    protected void onBindViewHolder(@NonNull PersistedFruit item,
                                    @NonNull OrangeHolder viewHolder,
                                    @NonNull List<Object> payloads) {
        viewHolder.getWeightView().setText(String.valueOf(item.weight()));
    }

    static class OrangeHolder extends RecyclerView.ViewHolder {

        private final TextView weightView;

        OrangeHolder(View itemView) {
            super(itemView);
            final ImageView photoView = itemView.findViewById(R.id.fruit_photo);
            photoView.setImageResource(R.drawable.ic_orange);
            final TextView nameView = itemView.findViewById(R.id.fruit_name);
            nameView.setText(FruitType.ORANGE.toString());
            this.weightView = itemView.findViewById(R.id.fruit_weight);
        }

        TextView getWeightView() {
            return weightView;
        }
    }

}
