package com.example.andrii_iliunin.a2_2_recyclerview.domain.user.repository;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.User;
import com.example.andrii_iliunin.a2_2_recyclerview.external.api.user.UserApi;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.Conversion;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.RandomUserRequestDto;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.UserDto;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;

class PlainUsersRepository implements UsersRepository {

    private static final int PAGE_SIZE = 10;

    private final UserApi api;
    private final Conversion<UserDto, User> networkConversion;

    PlainUsersRepository(final UserApi api, final Conversion<UserDto, User> networkConversion) {
        this.api = api;
        this.networkConversion = networkConversion;
    }

    @Override
    public Single<User> getRandom() {
        return this.api.getRandom()
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform)
                .flatMapObservable(Observable::fromIterable)
                .first(User.EMPTY);
    }

    @Override
    public Single<List<User>> getRandom(final int amount) {
        return this.api.getRandom()
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform);
    }

    @Override
    public Single<User> getRandom(final String gender) {
        return this.api.getRandom(gender)
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform)
                .flatMapObservable(Observable::fromIterable)
                .first(User.EMPTY);
    }

    @Override
    public Single<List<User>> getRandom(final int amount, final String gender) {
        return this.api.getRandom(gender)
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform);
    }

    @Override
    public Single<List<User>> getNext(final long page) {
        return this.api.getNext(page, PAGE_SIZE)
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform);
    }

    @Override
    public Single<List<User>> getNext(final String gender, final long page) {
        return this.api.getNext(gender, page, PAGE_SIZE)
                .map(RandomUserRequestDto::getUserResponse)
                .map(this.networkConversion::perform);
    }
}
