package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection;

import androidx.recyclerview.selection.ItemDetailsLookup;

public interface Detailed<T> {

    ItemDetailsLookup.ItemDetails<T> getItemDetails();

}
