package com.nyarian.android.cdp.game;

import com.nyarian.android.cdp.player.Player;

import java.util.Random;

public class RandomSignAssignmentStrategy implements SignAssignmentStrategy {

    @Override
    public void assign(final Player first, final Player second) {
        final Random random = new Random();
        final boolean mapping = random.nextBoolean();
        first.map(mapping ? Sign.CROSS : Sign.NOUGHT);
        second.map(mapping ? Sign.NOUGHT : Sign.CROSS);
    }
}
