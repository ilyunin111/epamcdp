package com.nyarian.android.cdp.game;

public class RestartGame implements Game {

    @Override
    public void start() {
        throw new UnsupportedOperationException("com.nyarian.android.cdp.game.RestartGame#start not yet implemented");
    }

    @Override
    public void reset() {
        throw new UnsupportedOperationException("com.nyarian.android.cdp.game.RestartGame#reset not yet implemented");
    }
}
