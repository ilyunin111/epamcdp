package com.nyarian.android.cdp.game;

import com.nyarian.android.cdp.player.Player;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Mockito.*;

public class RandomSignAssignmentStrategyTest implements TestOf<RandomSignAssignmentStrategy> {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    private Player first;
    @Mock
    private Player second;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void assign_verifyFirstPlayerGotValidSign() {
        this.subject().assign(this.first, this.second);
        verify(this.first).map(or(eq(Sign.CROSS), eq(Sign.NOUGHT)));
    }

    @Test
    public void assign_verifySecondPlayerGotValidSign() {
        this.subject().assign(this.first, this.second);
        verify(this.second).map(or(eq(Sign.CROSS), eq(Sign.NOUGHT)));
    }

    @Test
    public void assign_verifyFirstAndSecondPlayerWereAssignedWithDifferentSigns() {
        final Set<Sign> signSet = new HashSet<>();
        doAnswer(invocationOnMock -> {
            signSet.add(invocationOnMock.getArgument(0));
            return null;
        }).when(this.first).map(any());
        doAnswer(invocationOnMock -> {
            signSet.add(invocationOnMock.getArgument(0));
            return null;
        }).when(this.second).map(any());
        this.subject().assign(this.first, this.second);
        assertEquals(2, signSet.size());
    }

    @Override
    public RandomSignAssignmentStrategy subject() {
        return new RandomSignAssignmentStrategy();
    }
}