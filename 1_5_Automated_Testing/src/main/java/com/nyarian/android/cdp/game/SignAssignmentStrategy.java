package com.nyarian.android.cdp.game;

import com.nyarian.android.cdp.player.Player;

public interface SignAssignmentStrategy {

    void assign(Player first, Player second);

}
