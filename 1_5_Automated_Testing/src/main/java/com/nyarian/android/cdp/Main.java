package com.nyarian.android.cdp;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.PlainCombinationSearch;
import com.nyarian.android.cdp.game.PlainGame;
import com.nyarian.android.cdp.game.RandomSignAssignmentStrategy;
import com.nyarian.android.cdp.player.ConsolePlayer;
import com.nyarian.android.cdp.print.ConsoleGreeter;
import com.nyarian.android.cdp.print.ConsolePrinter;
import com.nyarian.android.cdp.print.PlainMessenger;
import com.nyarian.android.cdp.turn.RandomFirstTurnStrategy;
import com.nyarian.android.cdp.turn.StrategySignChecker;
import com.nyarian.android.cdp.turn.WinnerTurnsSecondStrategy;

public class Main {

    private static final int SIZE = 3;

    public static void main(final String[] args) {
        new PlainGame(
                new ConsolePlayer(),
                new ConsolePlayer(),
                new Board(SIZE),
                new ConsolePrinter(),
                new ConsoleGreeter(new PlainMessenger()),
                new PlainCombinationSearch(),
                new WinnerTurnsSecondStrategy(new RandomFirstTurnStrategy(), new StrategySignChecker()),
                new RandomSignAssignmentStrategy()
        ).start();
    }

}

