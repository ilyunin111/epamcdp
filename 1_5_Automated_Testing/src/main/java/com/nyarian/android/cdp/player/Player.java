package com.nyarian.android.cdp.player;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.Sign;

public interface Player {

    void turn(final Board board);

    void map(Sign sign);

    Sign sign();

}
