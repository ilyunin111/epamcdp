package com.nyarian.android.cdp.player;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.CellIsTakenException;
import com.nyarian.android.cdp.game.Position;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ConsolePlayer extends AbstractPlayer {

    private final Scanner scanner;

    public ConsolePlayer() {
        this.scanner = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
    }

    @Override
    public void turn(final Board board) {
        try {
            System.out.println("Please, enter your position.");
            final String enteredPosition = this.scanner.nextLine();
            final Position position = new Position(enteredPosition.split(" "));
            board.move(position, this.sign);
        } catch (final NumberFormatException e) {
            System.out.println("Incorrect input. Please, try again.");
            this.turn(board);
        } catch (final CellIsTakenException e) {
            System.out.println("This cell is already taken. Please, specify a valid one.");
            this.turn(board);
        }
    }

}
