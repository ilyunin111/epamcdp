package com.example.nyarian.adaptertask.domain;

public interface PersistedFruit extends Fruit {

    long getId();

}
