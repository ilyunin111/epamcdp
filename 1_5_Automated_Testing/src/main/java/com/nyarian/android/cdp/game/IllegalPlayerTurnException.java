package com.nyarian.android.cdp.game;

public class IllegalPlayerTurnException extends RuntimeException {

    public IllegalPlayerTurnException(final String message) {
        super(message);
    }
}
