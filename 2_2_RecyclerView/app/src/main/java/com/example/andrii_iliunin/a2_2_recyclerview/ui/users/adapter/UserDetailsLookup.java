package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import android.view.MotionEvent;
import android.view.View;

import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.selection.Detailed;

import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;

public class UserDetailsLookup extends ItemDetailsLookup<String> {

    private final RecyclerView recyclerView;

    public UserDetailsLookup(final RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @SuppressWarnings("unchecked")
    @Override
    public ItemDetails<String> getItemDetails(final MotionEvent e) {
        final View view = this.recyclerView.findChildViewUnder(e.getX(), e.getY());
        final RecyclerView.ViewHolder viewHolder = this.recyclerView.getChildViewHolder(view);
        if (viewHolder instanceof Detailed) {
            return ((Detailed<String>) viewHolder).getItemDetails();
        } else {
            throw new IllegalStateException("Undetailed item is in the list");
        }
    }
}
