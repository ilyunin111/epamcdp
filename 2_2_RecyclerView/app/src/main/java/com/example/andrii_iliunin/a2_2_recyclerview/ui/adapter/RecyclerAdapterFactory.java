package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

public interface RecyclerAdapterFactory {

    RecyclerAdapter create(final AbstractChainedBindStrategy bindStrategy, final ViewHolderFactory viewHolderFactory);

}
