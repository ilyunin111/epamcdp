package com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user;

import android.widget.ImageView;

import com.example.andrii_iliunin.a2_2_recyclerview.R;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.UiModel;
import com.squareup.picasso.Picasso;

public class PictureUi implements UiModel {

    private final String large;
    private final String medium;
    private final String thumbnail;

    public PictureUi(final String large, final String medium, final String thumbnail) {
        this.large = large;
        this.medium = medium;
        this.thumbnail = thumbnail;
    }

    public void loadLarge(final Picasso picasso, final ImageView destination) {
        this.loadImageInto(this.large, picasso, destination);
    }

    public void loadMedium(final Picasso picasso, final ImageView destination) {
        this.loadImageInto(this.medium, picasso, destination);
    }

    public void loadThumbnail(final Picasso picasso, final ImageView destination) {
        this.loadImageInto(this.thumbnail, picasso, destination);
    }

    private void loadImageInto(final String url, final Picasso picasso, final ImageView destination) {
        picasso.load(url).placeholder(R.drawable.ic_launcher_background).into(destination);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        final PictureUi pictureUi = (PictureUi) o;

        if (this.large != null ? !this.large.equals(pictureUi.large) : pictureUi.large != null) {
            return false;
        }
        if (this.medium != null ? !this.medium.equals(pictureUi.medium) : pictureUi.medium != null) {
            return false;
        }
        return this.thumbnail != null ? this.thumbnail.equals(pictureUi.thumbnail) : pictureUi.thumbnail == null;
    }

    @Override
    public int hashCode() {
        int result = this.large != null ? this.large.hashCode() : 0;
        result = 31 * result + (this.medium != null ? this.medium.hashCode() : 0);
        result = 31 * result + (this.thumbnail != null ? this.thumbnail.hashCode() : 0);
        return result;
    }
}
