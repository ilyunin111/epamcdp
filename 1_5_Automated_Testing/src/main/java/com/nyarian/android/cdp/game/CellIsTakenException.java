package com.nyarian.android.cdp.game;

public class CellIsTakenException extends RuntimeException {

    public CellIsTakenException(String message) {
        super(message);
    }
}
