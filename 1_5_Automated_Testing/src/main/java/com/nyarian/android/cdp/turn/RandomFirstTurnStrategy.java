package com.nyarian.android.cdp.turn;

import com.nyarian.android.cdp.game.Sign;
import com.nyarian.android.cdp.player.Player;

import java.util.Random;

public class RandomFirstTurnStrategy implements FirstTurnStrategy {

    @Override
    public Player whoTurnsFirst(final Player first, final Player second, final Sign lastWinner) {
        final Random random = new Random();
        return random.nextBoolean() ? first : second;
    }
}
