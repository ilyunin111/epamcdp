package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.Move;

import java.util.Collection;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class ConsolePrinter implements Printer {

    private static final String EMPTY_INNER_CELL = "_";
    private static final String EMPTY_OUTER_CELL = " ";
    private static final String DELIMITER = "|";

    @Override
    public void display(final Board board) {
        final Collection<Move> moves = board.moves();
        final StringBuilder builder = new StringBuilder();
        for (int i = 1, size = board.size(); i <= size; i++) {
            if (i != size) {
                builder.append(this.formLine(size, i, EMPTY_INNER_CELL, moves)).append(System.lineSeparator());
            } else {
                builder.append(this.formLine(size, i, EMPTY_OUTER_CELL, moves)).append(System.lineSeparator());
            }
        }
        System.out.println(builder.toString());
    }

    private String formLine(final int cells, final int row, final String cellType, final Collection<Move> moves) {
        final Deque<Move> stack = moves
                .stream()
                .filter(move -> move.position().getY() == row)
                .sorted(Comparator.comparingInt(o -> o.position().getY()))
                .collect(Collectors.toCollection(LinkedList::new));
        final StringBuilder builder;
        if (!stack.isEmpty() && stack.peekFirst().position().getX() == 1) {
            builder = new StringBuilder(stack.pollFirst().sign().character());
        } else {
            builder = new StringBuilder(cellType);
        }
        for (int i = 2; i < cells + 1; i++) {
            builder.append(DELIMITER);
            if (!stack.isEmpty() && stack.peekFirst().position().getX() == i) {
                builder.append(stack.pollFirst().sign().character());
            } else {
                builder.append(cellType);
            }
        }
        return builder.toString();
    }

}
