package com.example.nyarian.adaptertask.domain;

public interface Fruit {

    String name();

    int weight();

}
