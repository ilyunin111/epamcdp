package com.example.nyarian.adaptertask.domain;

import java.util.Collection;

public interface DataSource<T> {

    Collection<T> request();

}
