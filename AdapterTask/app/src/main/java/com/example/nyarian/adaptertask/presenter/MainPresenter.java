package com.example.nyarian.adaptertask.presenter;

import com.example.nyarian.adaptertask.MainView;
import com.example.nyarian.adaptertask.domain.DataSource;
import com.example.nyarian.adaptertask.domain.PersistedFruit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter {

    private final MainView view;
    private final DataSource<PersistedFruit> source;

    public MainPresenter(MainView view, DataSource<PersistedFruit> source) {
        this.view = view;
        this.source = source;
    }

    public void onFabPressed() {
        Single.just(this.source.request())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this.view::update);
    }

}
