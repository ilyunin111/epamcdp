package com.example.nyarian.adaptertask;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.nyarian.adaptertask.domain.FruitDataSource;
import com.example.nyarian.adaptertask.domain.PersistedFruit;
import com.example.nyarian.adaptertask.presenter.MainPresenter;

import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements MainView {

    private FruitsAdapter adapter;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.presenter = new MainPresenter(this, new FruitDataSource(new Random()));
        this.adapter = new FruitsAdapter(Collections.emptyList(), this);
        this.findViewById(R.id.fab).setOnClickListener(v -> this.presenter.onFabPressed());
        final RecyclerView recyclerView = this.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(this.adapter);
    }

    @Override
    public void update(Collection<PersistedFruit> fruits) {
        this.adapter.update(fruits);
    }
}
