package com.example.andrii_iliunin.a2_2_recyclerview.application.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.andrii_iliunin.a2_2_recyclerview.R;

import androidx.core.app.NotificationCompat;
import androidx.work.Worker;

public class NotificationWorker extends Worker {

    private static final String TITLE_KEY = "title";
    private static final String DESCRIPTION_KEY = "desc";

    @NonNull
    @Override
    public Result doWork() {
        final String title = this.getInputData().getString(TITLE_KEY);
        final String text = this.getInputData().getString(DESCRIPTION_KEY);
        this.sendNotification(title == null ? "Default" : title, text == null ? "Default" : text);
        return Result.SUCCESS;
    }

    private void sendNotification(final CharSequence title, final CharSequence message) {
        final NotificationManager notificationManager = (NotificationManager) this.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            final NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        final NotificationCompat.Builder notification = new NotificationCompat.Builder(this.getApplicationContext(), "default")
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManager.notify(1, notification.build());
    }
}
