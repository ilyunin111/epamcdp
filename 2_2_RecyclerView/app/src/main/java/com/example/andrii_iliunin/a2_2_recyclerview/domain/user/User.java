package com.example.andrii_iliunin.a2_2_recyclerview.domain.user;

public class User {

    public static final User EMPTY = new User("", "", "", Address.EMPTY, Picture.EMPTY, "");

    private final String id;
    private final String gender;
    private final String name;
    private final Address address;
    private final Picture picture;
    private final String email;

    public User(final String id,
                final String gender,
                final String name,
                final Address address,
                final Picture picture,
                final String email) {
        this.id = id == null ? "" : id;
        this.gender = gender;
        this.name = name;
        this.address = address;
        this.picture = picture;
        this.email = email;
    }

    public String getId() {
        return this.id;
    }

    public String getGender() {
        return this.gender;
    }

    public String getName() {
        return this.name;
    }

    public Address getAddress() {
        return this.address;
    }

    public Picture getPicture() {
        return this.picture;
    }

    public String getEmail() {
        return this.email;
    }
}
