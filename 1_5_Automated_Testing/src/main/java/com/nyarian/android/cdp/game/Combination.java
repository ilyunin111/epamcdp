package com.nyarian.android.cdp.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Combination {

    public static final Combination NO_COMBINATION = new Combination(
            Collections.emptyList(),
            Sign.NONE,
            false);

    private final List<Position> combination;
    private final Sign sign;
    private final boolean won;

    public Combination(final Collection<Position> combination, final Sign sign, final boolean won) {
        this.combination = new ArrayList<>(combination);
        this.sign = sign;
        this.won = won;
    }

    public List<Position> getCombination() {
        return new ArrayList<>(this.combination);
    }

    public boolean hasWon() {
        return this.won;
    }

    public Sign sign() {
        return this.sign;
    }

}
