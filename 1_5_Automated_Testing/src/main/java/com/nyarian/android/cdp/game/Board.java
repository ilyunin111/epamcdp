package com.nyarian.android.cdp.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Board {

    private final int size;
    private final List<Move> moves;
    private Sign signToTurnNext;

    public Board(final int size) {
        if (size <= 2) {
            throw new IllegalArgumentException();
        }
        this.size = size;
        this.moves = new ArrayList<>();
        this.signToTurnNext = Sign.NONE;
    }

    public Sign whoseTurn() {
        return this.signToTurnNext;
    }

    public Collection<Move> moves() {
        return new ArrayList<>(this.moves);
    }

    public Collection<Move> moves(final Sign sign) {
        return this.moves.stream().filter(move -> move.sign() == sign).collect(Collectors.toList());
    }

    public void move(final Position position, final Sign sign) {
        if (this.signToTurnNext != Sign.NONE && this.signToTurnNext != sign) {
            throw new IllegalPlayerTurnException(String.format("Next sign to turn is %s, but got %s", this.signToTurnNext, sign));
        }
        if (this.moves.stream().map(Move::position).collect(Collectors.toList()).contains(position)) {
            throw new CellIsTakenException("Cell " + position + " is already taken.");
        }
        this.moves.add(new Move(position, sign));
        this.signToTurnNext = sign.opposite();
    }

    public Move lastMove() {
        if (this.moves.isEmpty()) {
            throw new IllegalStateException("Can't retrieve last move because list is empty.");
        }
        return this.moves.get(this.moves.size() - 1);
    }

    public void reset() {
        this.moves.clear();
        this.signToTurnNext = Sign.NONE;
    }

    public Board duplicate() {
        final Board board = new Board(this.size);
        board.moves.addAll(this.moves);
        board.signToTurnNext = this.signToTurnNext;
        return board;
    }

    public int size() {
        return this.size;
    }
}
