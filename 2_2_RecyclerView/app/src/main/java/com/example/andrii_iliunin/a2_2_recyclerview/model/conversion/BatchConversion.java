package com.example.andrii_iliunin.a2_2_recyclerview.model.conversion;

import java.util.List;

import io.reactivex.Observable;

public abstract class BatchConversion<T, R> implements Conversion<T, R> {

    @Override
    public List<R> perform(final Iterable<T> source) {
        return Observable.fromIterable(source)
                .map(this::perform)
                .toList()
                .blockingGet();
    }
}
