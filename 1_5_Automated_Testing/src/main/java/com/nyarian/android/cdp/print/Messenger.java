package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.player.Player;

public interface Messenger {

    String message(final Player winner, final Player looser);

}
