package com.nyarian.android.cdp.player;

import com.nyarian.android.cdp.game.Sign;

public abstract class AbstractPlayer implements Player {

    protected Sign sign;

    @Override
    public void map(final Sign sign) {
        this.sign = sign;
    }

    @Override
    public Sign sign() {
        return this.sign;
    }

}
