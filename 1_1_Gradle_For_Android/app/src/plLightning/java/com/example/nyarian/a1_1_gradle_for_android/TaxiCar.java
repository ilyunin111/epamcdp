package com.example.nyarian.a1_1_gradle_for_android;

import io.reactivex.Single;

public class TaxiCar {

    private static final String TAXI_CAR_NAME = "Poland Lightning Taxi";

    public static String getTaxiCarName() {
        return Single.just(TAXI_CAR_NAME).blockingGet();
    }

}
