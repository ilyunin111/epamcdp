package com.nyarian.android.cdp.search;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.Position;
import com.nyarian.android.cdp.game.Sign;

public class DeepSearch implements MoveSearch {

    @Override
    public Position find(final Board board, final Sign sign) {
        throw new UnsupportedOperationException("com.nyarian.android.cdp.search.DeepSearch#find not yet implemented");
    }
}
