package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.data;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

import androidx.paging.DataSource;

public class UsersDataSourceFactory extends DataSource.Factory<Long, UserUi> {

    private final UsersUiRepository repository;

    public UsersDataSourceFactory(final UsersUiRepository repository) {
        this.repository = repository;
    }

    @Override
    public DataSource<Long, UserUi> create() {
        return new UsersDataSource(this.repository);
    }
}
