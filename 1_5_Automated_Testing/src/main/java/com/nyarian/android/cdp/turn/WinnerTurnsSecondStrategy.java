package com.nyarian.android.cdp.turn;

import com.nyarian.android.cdp.game.Sign;
import com.nyarian.android.cdp.player.Player;

public class WinnerTurnsSecondStrategy implements FirstTurnStrategy {

    private final FirstTurnStrategy fallbackStrategy;
    private final StrategySignChecker checker;

    public WinnerTurnsSecondStrategy(final FirstTurnStrategy fallbackStrategy, final StrategySignChecker checker) {
        this.fallbackStrategy = fallbackStrategy;
        this.checker = checker;
    }

    @Override
    public Player whoTurnsFirst(final Player first, final Player second, final Sign lastWinner) {
        this.checker.examineSigns(first, second, lastWinner);
        final Player firstTurningPlayer;
        if (lastWinner == Sign.NONE) {
            firstTurningPlayer = this.fallbackStrategy.whoTurnsFirst(first, second, lastWinner);
        } else {
            firstTurningPlayer = first.sign() == lastWinner ? second : first;
        }
        return firstTurningPlayer;
    }
}
