package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.player.Player;

public class ConsoleGreeter extends Greeter {

    public ConsoleGreeter(final Messenger messenger) {
        super(messenger);
    }

    @Override
    public void greet(final Player winner, final Player looser) {
        System.out.println(this.getMessenger().message(winner, looser));
    }
}
