package com.nyarian.android.cdp.turn;

import com.nyarian.android.cdp.game.Sign;
import com.nyarian.android.cdp.game.TestOf;
import com.nyarian.android.cdp.player.Player;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

public class WinnerTurnsSecondStrategyTest implements TestOf<WinnerTurnsSecondStrategy> {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    private FirstTurnStrategy fallbackStrategy;
    @Mock
    private Player first;
    @Mock
    private Player second;
    @Mock
    private StrategySignChecker checker;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void whoTurnsFirst_verifySignsWerePassedToChecker() {
        this.subject().whoTurnsFirst(this.first, this.second, Sign.NOUGHT);
        verify(this.checker).examineSigns(eq(this.first), eq(this.second), eq(Sign.NOUGHT));
    }

    @Test
    public void whoTurnsFirst_verifyFallbackStrategyWasUsed_andItReturnedFirstPlayer_ifNoLastWinnerIsRegistered() {
        doReturn(this.first).when(this.fallbackStrategy).whoTurnsFirst(any(), any(), any());
        assertSame(this.first, this.subject().whoTurnsFirst(this.first, this.second, Sign.NONE));
    }

    @Test
    public void whoTurnsFirst_verifyFirstPlayerReturned_ifLastWinnerIsNoughts_andFirstPlayerPlaysCrosses() {
        doReturn(Sign.CROSS).when(this.first).sign();
        assertSame(this.first, this.subject().whoTurnsFirst(this.first, this.second, Sign.NOUGHT));
    }

    @Test
    public void whoTurnsFirst_verifySecondPlayerReturned_ifLastWinnerIsCrosses_andFirstPlayerPlaysCrosses() {
        doReturn(Sign.CROSS).when(this.first).sign();
        assertSame(this.second, this.subject().whoTurnsFirst(this.first, this.second, Sign.CROSS));
    }

    @Override
    public WinnerTurnsSecondStrategy subject() {
        return new WinnerTurnsSecondStrategy(this.fallbackStrategy, this.checker);
    }
}