package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

import androidx.paging.PagedList;
import androidx.recyclerview.selection.ItemKeyProvider;
import androidx.recyclerview.widget.RecyclerView;

public class UserKeyProvider extends ItemKeyProvider<String> {

    public static final String KEY_PROGRESS = "PROGRESS";

    private final UsersListAdapter adapter;

    public UserKeyProvider(final UsersListAdapter adapter) {
        super(ItemKeyProvider.SCOPE_CACHED);
        this.adapter = adapter;
    }

    @Override
    public String getKey(final int position) {
        if (position == this.adapter.getItemCount() - 1) {
            return KEY_PROGRESS;
        } else {
            return this.adapter.get(position).getId();
        }
    }

    @Override
    public int getPosition(final String key) {
        final PagedList<UserUi> snapshot = this.adapter.getCurrentList();
        for (int i = 0, snapshotSize = snapshot.size(); i < snapshotSize; i++) {
            final UserUi userUi = snapshot.get(i);
            if (userUi.getId().equals(key)) {
                return i;
            }
        }
        return RecyclerView.NO_POSITION;
    }
}
