package com.nyarian.android.cdp.game;

public interface Game {

    void start();

    void reset();

}
