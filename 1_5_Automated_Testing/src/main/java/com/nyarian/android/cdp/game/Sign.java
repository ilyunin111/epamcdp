package com.nyarian.android.cdp.game;

public enum Sign {

    NONE {
        @Override
        public Sign opposite() {
            throw new IllegalStateException("No opposite for absent sign");
        }

        @Override
        public String character() {
            throw new UnsupportedOperationException("#character not implemented");
        }
    },
    CROSS {
        @Override
        public Sign opposite() {
            return NOUGHT;
        }

        @Override
        public String character() {
            return "X";
        }
    }, NOUGHT {
        @Override
        public Sign opposite() {
            return CROSS;
        }

        @Override
        public String character() {
            return "O";
        }
    };

    public abstract Sign opposite();

    public abstract String character();
}
