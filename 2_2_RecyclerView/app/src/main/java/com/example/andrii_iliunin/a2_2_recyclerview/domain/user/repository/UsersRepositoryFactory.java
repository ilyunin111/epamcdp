package com.example.andrii_iliunin.a2_2_recyclerview.domain.user.repository;

import com.example.andrii_iliunin.a2_2_recyclerview.external.api.user.UserApiFactory;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.user.UserNetworkToDomainConversion;

public class UsersRepositoryFactory {

    public UsersRepository getRepository() {
        return new PlainUsersRepository(new UserApiFactory().create(),
                new UserNetworkToDomainConversion());
    }

}
