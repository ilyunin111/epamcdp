package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import java.util.List;

public interface ChainedBindStrategy {

    void bind(final RecyclerViewHolder recyclerViewHolder, final int position, final List<Object> payload);

}
