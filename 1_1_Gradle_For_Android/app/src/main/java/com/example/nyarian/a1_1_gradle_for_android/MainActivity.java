package com.example.nyarian.a1_1_gradle_for_android;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.flag).setOnClickListener(view ->
                Toast.makeText(this, TaxiCar.getTaxiCarName(), Toast.LENGTH_LONG).show());
    }
}
