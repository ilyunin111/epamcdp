package com.nyarian.android.cdp.game;

public class Position {

    private final int x;
    private final int y;

    public Position(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public Position(final String[] position) {
        this.x = Integer.parseInt(position[0]);
        this.y = Integer.parseInt(position[1]);
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

}
