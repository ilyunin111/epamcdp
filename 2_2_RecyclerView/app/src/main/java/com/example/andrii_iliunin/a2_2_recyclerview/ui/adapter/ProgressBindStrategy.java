package com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter;

import java.util.List;

import androidx.lifecycle.LiveData;

public class ProgressBindStrategy extends AbstractChainedBindStrategy {

    private final LiveData<Boolean> progressStatus;

    public ProgressBindStrategy(final LiveData<Boolean> progressStatus) {
        super();
        this.progressStatus = progressStatus;
    }

    public ProgressBindStrategy(final ChainedBindStrategy next,
                                final LiveData<Boolean> progressStatus) {
        super(next);
        this.progressStatus = progressStatus;
    }

    @Override
    protected boolean isSuitable(final int viewType) {
        return viewType == RecyclerViewHolder.PROGRESS_VIEW_TYPE;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void process(final RecyclerViewHolder recyclerViewHolder,
                           final int position,
                           final List<Object> payload) {
        recyclerViewHolder.bind(this.progressStatus.getValue(), payload);
    }

}