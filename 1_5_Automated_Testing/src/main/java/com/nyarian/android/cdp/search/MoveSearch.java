package com.nyarian.android.cdp.search;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.Position;
import com.nyarian.android.cdp.game.Sign;

public interface MoveSearch {

    Position find(Board board, final Sign sign);

}
