package com.example.nyarian.a1_1_gradle_for_android;

import org.apache.commons.lang3.StringUtils;

public class TaxiCar {

    private static final String TAXI_CAR_NAME = "Ukrainian Lovely Taxi";

    public static String getTaxiCarName() {
        return StringUtils.join(TAXI_CAR_NAME.split(" "));
    }

}
