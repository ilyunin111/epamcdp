package com.nyarian.android.cdp.turn;

import com.nyarian.android.cdp.game.Sign;
import com.nyarian.android.cdp.player.Player;

public interface FirstTurnStrategy {

    Player whoTurnsFirst(final Player first, final Player second, final Sign lastWinner);

}
