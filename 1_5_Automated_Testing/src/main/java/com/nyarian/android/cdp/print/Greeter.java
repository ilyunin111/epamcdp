package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.player.Player;

public abstract class Greeter {

    private final Messenger messenger;

    Greeter(Messenger messenger) {
        this.messenger = messenger;
    }

    public abstract void greet(final Player winner, final Player looser);

    public Messenger getMessenger() {
        return messenger;
    }
}
