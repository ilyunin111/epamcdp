package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.game.Board;

public interface Printer {

    void display(final Board board);

}
