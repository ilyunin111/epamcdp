package com.example.andrii_iliunin.a2_2_recyclerview.model.conversion;

import java.util.List;

public interface Conversion<T, R> {

    R perform(T source);

    List<R> perform(Iterable<T> source);

}
