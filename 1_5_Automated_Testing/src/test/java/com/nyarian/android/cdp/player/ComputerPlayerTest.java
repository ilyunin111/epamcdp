package com.nyarian.android.cdp.player;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.game.TestOf;
import com.nyarian.android.cdp.search.MoveSearch;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.quality.Strictness;

public class ComputerPlayerTest implements TestOf<ComputerPlayer> {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule().strictness(Strictness.STRICT_STUBS);

    @Mock
    private MoveSearch moveSearch;
    @Mock
    private Board board;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Override
    public ComputerPlayer subject() {
        return new ComputerPlayer(this.moveSearch);
    }
}