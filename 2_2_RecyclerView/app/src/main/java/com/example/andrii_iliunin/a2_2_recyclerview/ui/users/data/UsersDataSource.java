package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.data;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;

import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

import androidx.paging.PageKeyedDataSource;
import io.reactivex.disposables.CompositeDisposable;

public class UsersDataSource extends PageKeyedDataSource<Long, UserUi> {

    private static final long INITIAL_PAGE = 1L;
    private static final long NEXT_PAGE_INCREMENT = 1L;

    private final UsersUiRepository repository;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    UsersDataSource(final UsersUiRepository repository) {
        this.repository = repository;
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadInitial(@NonNull final LoadInitialParams<Long> params, @NonNull final LoadInitialCallback<Long, UserUi> callback) {
        this.repository.getNext(INITIAL_PAGE)
                .doOnSubscribe(this.compositeDisposable::add)
                .subscribe(users ->
                                callback.onResult(users,
                                        null,
                                        INITIAL_PAGE + NEXT_PAGE_INCREMENT),
                        throwable -> {

                        });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Long> params, @NonNull final LoadCallback<Long, UserUi> callback) {
        throw new UnsupportedOperationException("#loadBefore()");
    }

    @SuppressLint("CheckResult")
    @Override
    public void loadAfter(@NonNull final LoadParams<Long> params, @NonNull final LoadCallback<Long, UserUi> callback) {
        this.repository.getNext(params.key)
                .doOnSubscribe(this.compositeDisposable::add)
                .subscribe(users -> callback.onResult(users, !users.isEmpty() ?
                                params.key + NEXT_PAGE_INCREMENT : null),
                        throwable -> {

                        });
    }
}
