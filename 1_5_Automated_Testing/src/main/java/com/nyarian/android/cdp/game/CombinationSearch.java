package com.nyarian.android.cdp.game;

import java.util.Collection;

public interface CombinationSearch {

    Combination find(final Collection<Position> positions, final Position lastPosition, Sign sign, int boardSize);

}
