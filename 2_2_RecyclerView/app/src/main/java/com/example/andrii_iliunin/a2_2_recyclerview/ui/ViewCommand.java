package com.example.andrii_iliunin.a2_2_recyclerview.ui;

public interface ViewCommand {

    void execute();

    boolean canExecute();

}
