package com.example.nyarian.adaptertask;

import android.app.Activity;

import com.example.nyarian.adaptertask.domain.PersistedFruit;
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter;

import java.util.Collection;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FruitsAdapter extends ListDelegationAdapter<List<PersistedFruit>> {

    FruitsAdapter(final List<PersistedFruit> plainFruits, final Activity activity) {
        this.setHasStableIds(true);
        this.delegatesManager.addDelegate(new AppleDrawer(activity));
        this.delegatesManager.addDelegate(new OrangeDrawer(activity));
        this.delegatesManager.addDelegate(new BananaDrawer(activity));
        this.setItems(plainFruits);
    }

    void update(final Collection<PersistedFruit> plainFruits) {
        Observable.merge(Observable.fromIterable(this.getItems()),
                Observable.fromIterable(plainFruits))
                .distinct()
                .sorted()
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateItems);
    }

    @Override
    public long getItemId(int position) {
        return this.getItems().get(position).getId();
    }

    private void updateItems(List<PersistedFruit> items) {
        this.setItems(items);
        this.notifyDataSetChanged();
    }
}
