package com.nyarian.android.cdp.print;

import com.nyarian.android.cdp.player.Player;

import java.util.function.UnaryOperator;

public class PlainMessenger implements Messenger {

    private static final UnaryOperator<String> TYPICAL_WINNER_GREETING = name -> String.format("Congratulations, %s!", name);

    @Override
    public String message(final Player winner, final Player looser) {
        return TYPICAL_WINNER_GREETING.apply(winner.toString());
    }
}
