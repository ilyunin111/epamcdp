package com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.user;

import android.support.annotation.NonNull;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.Address;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.Picture;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.User;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.BatchConversion;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.AddressUi;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.PictureUi;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

public class UserDomainToUiConversion extends BatchConversion<User, UserUi> {

    @Override
    public UserUi perform(final User source) {
        final Picture picture = source.getPicture();
        final Address address = source.getAddress();
        return new UserUi(source.getId(), source.getName(), this.formPictureUi(picture), this.formAddressUi(address));
    }

    @NonNull
    private AddressUi formAddressUi(final Address address) {
        return new AddressUi(
                address.getState(),
                address.getCity(),
                address.getStreet()
        );
    }

    @NonNull
    private PictureUi formPictureUi(final Picture picture) {
        return new PictureUi(
                picture.getLargePictureUrl(),
                picture.getMediumPictureUrl(),
                picture.getThumbnailPictureUrl()
        );
    }
}
