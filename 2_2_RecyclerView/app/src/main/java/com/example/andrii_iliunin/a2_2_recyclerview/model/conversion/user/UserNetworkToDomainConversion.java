package com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.user;

import android.support.annotation.NonNull;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.Address;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.Coordinates;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.Picture;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.User;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.BatchConversion;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.CoordinatesUserAddressDto;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.LocationDto;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.NameDto;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.PictureDto;
import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.UserDto;

import java.math.BigDecimal;

public class UserNetworkToDomainConversion extends BatchConversion<UserDto, User> {

    @Override
    public User perform(final UserDto source) {
        final LocationDto locationResponse = source.getLocationDto();
        final Coordinates coordinates = this.formCoordinates(locationResponse.getCoordinatesUserAddressDto());
        final Address address = this.formAddress(locationResponse, coordinates);
        final Picture picture = this.formPicture(source.getPictureDto());
        final String userName = this.formName(source.getNameDto());
        final String id = this.retrieveId(source);
        return new User(id, source.getGender(), userName, address, picture, source.getEmail());
    }

    private String retrieveId(final UserDto source) {
        return source.getIdDto().getValue();
    }

    private String formName(final NameDto nameResponse) {
        return String.format("%s %s %s", nameResponse.getTitle(), nameResponse.getFirst(), nameResponse.getLast());
    }

    @NonNull
    private Coordinates formCoordinates(final CoordinatesUserAddressDto coordinatesResponse) {
        final String latitudeResponse = coordinatesResponse.getLatitude();
        final String longitudeResponse = coordinatesResponse.getLongitude();
        return new Coordinates(new BigDecimal(latitudeResponse), new BigDecimal(longitudeResponse));
    }

    @NonNull
    private Address formAddress(final LocationDto response, final Coordinates coordinates) {
        return new Address(response.getStreet(),
                response.getCity(),
                response.getState(),
                response.getPostcode(),
                coordinates);
    }

    private Picture formPicture(final PictureDto pictureDto) {
        return new Picture(pictureDto.getLarge(), pictureDto.getMedium(), pictureDto.getThumbnail());
    }

}
