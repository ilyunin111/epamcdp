
package com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDto {

    @SerializedName("name")
    @Expose
    private NameDto nameDto;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("location")
    @Expose
    private LocationDto locationDto;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("id")
    @Expose
    private IdDto idDto;
    @SerializedName("picture")
    @Expose
    private PictureDto pictureDto;

    public String getGender() {
        return this.gender;
    }

    public void setGender(final String gender) {
        this.gender = gender;
    }

    public NameDto getNameDto() {
        return this.nameDto;
    }

    public void setNameDto(final NameDto nameDto) {
        this.nameDto = nameDto;
    }

    public LocationDto getLocationDto() {
        return this.locationDto;
    }

    public void setLocationDto(final LocationDto locationDto) {
        this.locationDto = locationDto;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public IdDto getIdDto() {
        return this.idDto;
    }

    public void setIdDto(final IdDto idDto) {
        this.idDto = idDto;
    }

    public PictureDto getPictureDto() {
        return this.pictureDto;
    }

    public void setPictureDto(final PictureDto pictureDto) {
        this.pictureDto = pictureDto;
    }

}
