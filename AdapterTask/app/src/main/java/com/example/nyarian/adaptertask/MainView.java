package com.example.nyarian.adaptertask;

import com.example.nyarian.adaptertask.domain.PersistedFruit;

import java.util.Collection;

public interface MainView {

    void update(final Collection<PersistedFruit> fruits);

}
