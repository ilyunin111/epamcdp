package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.andrii_iliunin.a2_2_recyclerview.R;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.ProgressViewHolder;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.RecyclerViewHolder;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.adapter.ViewHolderFactory;

public class UsersViewHolderFactory implements ViewHolderFactory {

    @Override
    public RecyclerViewHolder createHolder(final ViewGroup parent, final int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case RecyclerViewHolder.PROGRESS_VIEW_TYPE:
                return new ProgressViewHolder(inflater.inflate(R.layout.item_progress, parent, false));
            case UsersListAdapter.USER_VIEW_TYPE:
                return new UserViewHolder(inflater.inflate(R.layout.item_user, parent, false));
            default:
                throw new IllegalArgumentException("Unknown view type: " + viewType);
        }
    }
}
