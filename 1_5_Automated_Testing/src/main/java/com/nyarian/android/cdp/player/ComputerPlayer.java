package com.nyarian.android.cdp.player;

import com.nyarian.android.cdp.game.Board;
import com.nyarian.android.cdp.search.MoveSearch;

public class ComputerPlayer extends AbstractPlayer {

    private final MoveSearch moveSearch;

    public ComputerPlayer(final MoveSearch moveSearch) {
        this.moveSearch = moveSearch;
    }

    @Override
    public void turn(final Board board) {
        throw new UnsupportedOperationException("com.nyarian.android.cdp.player.ComputerPlayer#turn not yet implemented");
    }

}
