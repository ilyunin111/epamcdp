package com.example.andrii_iliunin.a2_2_recyclerview.domain.user.repository;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.User;

import java.util.List;

import io.reactivex.Single;

public interface UsersRepository {

    Single<User> getRandom();

    Single<List<User>> getRandom(final int amount);

    Single<User> getRandom(final String gender);

    Single<List<User>> getRandom(final int amount, final String gender);

    Single<List<User>> getNext(final long page);

    Single<List<User>> getNext(final String gender, final long page);

}
