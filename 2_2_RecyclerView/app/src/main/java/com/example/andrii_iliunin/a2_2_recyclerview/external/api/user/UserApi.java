package com.example.andrii_iliunin.a2_2_recyclerview.external.api.user;

import com.example.andrii_iliunin.a2_2_recyclerview.model.dto.user.RandomUserRequestDto;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UserApi {

    @GET
    Single<RandomUserRequestDto> getRandom();

    @GET
    Single<RandomUserRequestDto> getRandom(@Query("results") final int amount);

    @GET
    Single<RandomUserRequestDto> getRandom(@Query("gender") final String gender);

    @GET
    Single<RandomUserRequestDto> getRandom(@Query("results") final int amount, @Query("gender") final String gender);

    @GET("?seed=foobar")
    Single<RandomUserRequestDto> getNext(@Query("page") final long page, @Query("results") final int pageSize);

    @GET("?seed=foobar")
    Single<RandomUserRequestDto> getNext(@Query("gender") final String gender, @Query("page") final long page, @Query("results") final int pageSize);

}
