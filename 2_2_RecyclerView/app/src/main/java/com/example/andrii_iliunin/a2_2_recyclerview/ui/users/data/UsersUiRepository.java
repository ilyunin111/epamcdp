package com.example.andrii_iliunin.a2_2_recyclerview.ui.users.data;

import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.repository.UsersRepository;
import com.example.andrii_iliunin.a2_2_recyclerview.domain.user.User;
import com.example.andrii_iliunin.a2_2_recyclerview.model.conversion.Conversion;
import com.example.andrii_iliunin.a2_2_recyclerview.model.ui.user.UserUi;

import java.util.List;

import io.reactivex.Single;

public class UsersUiRepository {

    private final UsersRepository usersRepository;
    private final Conversion<User, UserUi> uiConversion;

    public UsersUiRepository(final UsersRepository usersRepository, final Conversion<User, UserUi> uiConversion) {
        this.usersRepository = usersRepository;
        this.uiConversion = uiConversion;
    }

    public Single<UserUi> getRandom() {
        return this.usersRepository.getRandom()
                .map(this.uiConversion::perform);
    }

    public Single<List<UserUi>> getRandom(final int amount) {
        return this.usersRepository.getRandom(amount)
                .map(this.uiConversion::perform);
    }

    public Single<UserUi> getRandom(final String gender) {
        return this.usersRepository.getRandom(gender)
                .map(this.uiConversion::perform);
    }

    public Single<List<UserUi>> getRandom(final int amount, final String gender) {
        return this.usersRepository.getRandom(amount, gender)
                .map(this.uiConversion::perform);
    }

    Single<List<UserUi>> getNext(final long page) {
        return this.usersRepository.getNext(page)
                .map(this.uiConversion::perform);
    }

    public Single<List<UserUi>> getNext(final String gender, final long page) {
        return this.usersRepository.getNext(gender, page)
                .map(this.uiConversion::perform);
    }
}
