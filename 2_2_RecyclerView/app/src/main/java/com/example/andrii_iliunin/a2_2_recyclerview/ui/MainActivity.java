package com.example.andrii_iliunin.a2_2_recyclerview.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;

import com.example.andrii_iliunin.a2_2_recyclerview.R;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.UsersViewModel;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter.UserDetailsLookup;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter.UserKeyProvider;
import com.example.andrii_iliunin.a2_2_recyclerview.ui.users.adapter.UsersListAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private UsersViewModel viewModel;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        this.viewModel = ViewModelProviders.of(this).get(UsersViewModel.class);
        final RecyclerView recyclerView = this.findViewById(R.id.content_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final UsersListAdapter adapter = new UsersListAdapter(this.viewModel.getLoadingStatusLiveData());
        recyclerView.setAdapter(adapter);
        final SelectionTracker<String> tracker = new SelectionTracker.Builder<>("selectionId",
                recyclerView,
                new UserKeyProvider(adapter),
                new UserDetailsLookup(recyclerView),
                StorageStrategy.createStringStorage())
                .withSelectionPredicate(this.createDefaultPredicate())
                .build();
        adapter.setTracker(tracker);
        this.viewModel.getUsersListLiveData().observe(this, adapter::submitList);
    }

    @NonNull
    private SelectionTracker.SelectionPredicate<String> createDefaultPredicate() {
        return new SelectionTracker.SelectionPredicate<String>() {
            @Override
            public boolean canSetStateForKey(final String key, final boolean nextState) {
                return true;
            }

            @Override
            public boolean canSetStateAtPosition(final int position, final boolean nextState) {
                return true;
            }

            @Override
            public boolean canSelectMultiple() {
                return true;
            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.remind_later) {
            this.viewModel.title("Sample title");
            this.viewModel.description("Sample description");
            this.viewModel.remindCommand().execute();
            return true;
        }
        return false;
    }
}
