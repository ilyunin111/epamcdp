package com.nyarian.android.cdp.game;

import java.util.Objects;

public class Move {

    private final Position position;
    private final Sign sign;

    public Move(final Position position, final Sign sign) {
        this.position = position;
        this.sign = sign;
    }

    public boolean isTop() {
        throw new UnsupportedOperationException();
    }

    public boolean isBottom() {
        throw new UnsupportedOperationException();
    }

    public boolean isLeft() {
        throw new UnsupportedOperationException();
    }

    public boolean isRight() {
        throw new UnsupportedOperationException();
    }

    public boolean isCenterHorizontal() {
        throw new UnsupportedOperationException();
    }

    public boolean isCenterVertical() {
        throw new UnsupportedOperationException();
    }

    public Sign sign() {
        return this.sign;
    }

    public Position position() {
        return this.position;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        final Move move = (Move) o;
        return Objects.equals(this.position, move.position) && this.sign == move.sign;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.position, this.sign);
    }
}
