package com.nyarian.android.cdp.game;

public interface TestOf<T> {

    T subject();

}
